﻿namespace TestWork
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeFiles = new System.Windows.Forms.TreeView();
            this.txtBoxText = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFindDir = new System.Windows.Forms.Button();
            this.txtBoxNameDir = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBoxPatternFile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.txtBoxInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtBoxCurrentFile = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeFiles
            // 
            this.treeFiles.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeFiles.Location = new System.Drawing.Point(0, 0);
            this.treeFiles.Name = "treeFiles";
            this.treeFiles.Size = new System.Drawing.Size(286, 536);
            this.treeFiles.TabIndex = 3;
            // 
            // txtBoxText
            // 
            this.txtBoxText.Location = new System.Drawing.Point(96, 63);
            this.txtBoxText.MaxLength = 255;
            this.txtBoxText.Multiline = true;
            this.txtBoxText.Name = "txtBoxText";
            this.txtBoxText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBoxText.Size = new System.Drawing.Size(414, 112);
            this.txtBoxText.TabIndex = 2;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(516, 79);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(110, 49);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Начать";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(516, 133);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(110, 42);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Остановить";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnFindDir);
            this.groupBox1.Controls.Add(this.txtBoxNameDir);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtBoxPatternFile);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btnStop);
            this.groupBox1.Controls.Add(this.btnStart);
            this.groupBox1.Controls.Add(this.txtBoxText);
            this.groupBox1.Location = new System.Drawing.Point(286, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(827, 283);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры поиска";
            // 
            // btnFindDir
            // 
            this.btnFindDir.Location = new System.Drawing.Point(516, 252);
            this.btnFindDir.Name = "btnFindDir";
            this.btnFindDir.Size = new System.Drawing.Size(75, 23);
            this.btnFindDir.TabIndex = 10;
            this.btnFindDir.Text = "Обзор";
            this.btnFindDir.UseVisualStyleBackColor = true;
            this.btnFindDir.Click += new System.EventHandler(this.btnFindDir_Click);
            // 
            // txtBoxNameDir
            // 
            this.txtBoxNameDir.Location = new System.Drawing.Point(99, 252);
            this.txtBoxNameDir.Name = "txtBoxNameDir";
            this.txtBoxNameDir.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtBoxNameDir.Size = new System.Drawing.Size(411, 22);
            this.txtBoxNameDir.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(99, 231);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "Укажите директорию";
            // 
            // txtBoxPatternFile
            // 
            this.txtBoxPatternFile.Location = new System.Drawing.Point(99, 201);
            this.txtBoxPatternFile.Name = "txtBoxPatternFile";
            this.txtBoxPatternFile.Size = new System.Drawing.Size(411, 22);
            this.txtBoxPatternFile.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(96, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Шаблон имени файла";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(93, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Текст, который ищем в файле";
            // 
            // lblTimer
            // 
            this.lblTimer.AutoSize = true;
            this.lblTimer.Location = new System.Drawing.Point(134, 34);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(0, 17);
            this.lblTimer.TabIndex = 14;
            // 
            // txtBoxInput
            // 
            this.txtBoxInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxInput.Location = new System.Drawing.Point(137, 111);
            this.txtBoxInput.Multiline = true;
            this.txtBoxInput.Name = "txtBoxInput";
            this.txtBoxInput.ReadOnly = true;
            this.txtBoxInput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtBoxInput.Size = new System.Drawing.Size(668, 116);
            this.txtBoxInput.TabIndex = 11;
            this.txtBoxInput.WordWrap = false;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Текущий файл:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Время:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Список файлов:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtBoxCurrentFile);
            this.groupBox2.Controls.Add(this.lblTimer);
            this.groupBox2.Controls.Add(this.txtBoxInput);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(286, 289);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(827, 247);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Результаты поиска";
            // 
            // txtBoxCurrentFile
            // 
            this.txtBoxCurrentFile.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBoxCurrentFile.Location = new System.Drawing.Point(137, 65);
            this.txtBoxCurrentFile.Multiline = true;
            this.txtBoxCurrentFile.Name = "txtBoxCurrentFile";
            this.txtBoxCurrentFile.ReadOnly = true;
            this.txtBoxCurrentFile.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtBoxCurrentFile.Size = new System.Drawing.Size(662, 45);
            this.txtBoxCurrentFile.TabIndex = 15;
            this.txtBoxCurrentFile.WordWrap = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 536);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.treeFiles);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1144, 583);
            this.MinimumSize = new System.Drawing.Size(1144, 583);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TreeView treeFiles;
        private System.Windows.Forms.TextBox txtBoxText;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBoxNameDir;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnFindDir;
        private System.Windows.Forms.TextBox txtBoxInput;
        private System.Windows.Forms.TextBox txtBoxPatternFile;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtBoxCurrentFile;
    }
}

