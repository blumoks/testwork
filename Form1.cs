﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace TestWork
{
    public partial class Form1 : Form
    {
        private delegate void UpdateList(string message);
        private delegate void UpdateTree(string node);
        private delegate void UpdateTime(string time);
        private static ManualResetEvent _stop = new ManualResetEvent(false);
        private static ManualResetEvent _pause = new ManualResetEvent(true);
        
        private bool IsRun;
        private Thread ThreadFindFile;
        private Thread ThreadTime;
       

        public Form1()
        {
            InitializeComponent();
            txtBoxText.Text = Properties.Settings.Default.SaveText;
            txtBoxPatternFile.Text = Properties.Settings.Default.SavePattern ;
            txtBoxNameDir.Text = Properties.Settings.Default.SavePath;
            btnStop.Enabled = false;
            IsRun = false;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {      
            Properties.Settings.Default.SaveText = txtBoxText.Text;
            Properties.Settings.Default.SavePattern = txtBoxPatternFile.Text;
            Properties.Settings.Default.SavePath = txtBoxNameDir.Text;
            Properties.Settings.Default.Save();
        }

        private void btnFindDir_Click(object sender, EventArgs e)
        {
            
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            FBD.ShowNewFolderButton = false;
            if (FBD.ShowDialog() == DialogResult.OK)
                txtBoxNameDir.Text = FBD.SelectedPath;

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if(IsRun)
            {
                _stop.Set();
                _pause.Set();
                btnStop.Text = "Остановить";
                ThreadFindFile.Join();
                ThreadTime.Join();
                _stop.Reset();
            }

            IsRun = true;
            treeFiles.Nodes.Clear();
            txtBoxInput.Clear();
            txtBoxCurrentFile.Text = "";
            btnStop.Enabled = true;

            if (Directory.Exists(txtBoxNameDir.Text))
            {
                btnStart.Enabled = false;
                ThreadFindFile = new Thread(threadWorkWithFile);
                ThreadFindFile.Start();

                ThreadTime = new Thread(ProgTimer);
                ThreadTime.Start();
            }
            else
                MessageBox.Show("Файл не найден");
        }


        private void btnStop_Click(object sender, EventArgs e)
        {
            if (btnStop.Text == "Продолжить")
            {
                btnStart.Enabled = false;
                btnStop.Text = "Остановить";
                _pause.Set();   
            }
            else
            {  
                btnStop.Text = "Продолжить";
                btnStart.Enabled = true;
                _pause.Reset();
            }
        }

        void addListBox(String message)
        {
            txtBoxInput.Text += message + Environment.NewLine;
            txtBoxCurrentFile.Text = message;
           
        }

        void Timer(String message)
        {
            lblTimer.Text = message;
        }
    

        private void ProgTimer()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            while(true)
            {
                TimeSpan ts = stopWatch.Elapsed;
                
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}",
                   ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                UpdateTime time = Timer;
                Invoke(time, new object[] { elapsedTime });
                _pause.WaitOne();
                if (_stop.WaitOne(0))
                {
                    return;
                }
            }
        }

        private int FindSubText(int pos, byte[] buf, byte[] arrText)
        {
            _pause.WaitOne();
            if (_stop.WaitOne(0))
            {
                throw new Exception();
            }
                
    
            for (int i = pos; i < buf.Length; i++)
            {
                if (buf[i] == arrText[pos])
                {
                    pos++;
                    if (pos == arrText.Length)
                        break;
                }
                else
                {
                    if (pos > 0)
                        i = i - pos;
                    pos = 0;
                }
            }
            return pos;
        }

        private void CheckFile(string pathToFile)
        {
            UpdateList addList = addListBox;
            Invoke(addList, new object[] { pathToFile });
            try
            {
                byte[] arrFile = File.ReadAllBytes(pathToFile);
                byte[] arrText = Encoding.ASCII.GetBytes(txtBoxText.Text);

                FileInfo file = new FileInfo(pathToFile);
                
                int pos = 0;

                if (arrFile.Length >= arrText.Length && arrText.Length != 0)
                {
                    if (file.Length / 1024 / 1024 <= 2)
                    {
                        pos = FindSubText(pos, arrFile, arrText);
                    }
                    else
                    {
                        using (BinaryReader br = new BinaryReader(new FileStream(pathToFile, FileMode.Open)))
                        {
                            byte[] buf = new byte[1024];

                            int positionFile = 0;
                            int count = 1024;
                            int bytesRead;
                            while ((bytesRead = br.Read(buf, 0, count)) > 0)
                            {
                                byte[] buf_ = new byte[bytesRead + pos];
                                positionFile += bytesRead;
                                if (pos > 0)
                                    Array.Copy(buf, 0, buf_, 0, pos);

                                Array.Copy(buf, 0, buf_, pos, bytesRead);

                                pos = FindSubText(pos, buf_, arrText);
                            }
                        }
                    }
                }

                if (pos == txtBoxText.Text.Length)
                {
                    UpdateTree add = AddNode;
                    Invoke(add, new object[] { pathToFile });
                }

            }
            catch
            {

            }
           
        }

        private void DoOp(ref Stack<string>  dirs, ref Stack<string>  files)
        {
            if (files.Count != 0)
            {
                
                CheckFile(files.Peek());
                files.Pop();
            }
            else
            {
                // список файлов пуст, но из цикла снаружи мы не вышли, значит в списке папок что-то есть
                var d = dirs.Peek();
                dirs.Pop();

                var addDirs = Directory.GetDirectories(d);
               
                    foreach (var dir_ in addDirs)
                        dirs.Push(dir_);


                    string pattern = txtBoxPatternFile.Text;
                    if (pattern == "")
                        pattern = "*.*";
                    var addFiles = Directory.GetFiles(d, pattern);
                    foreach (var f in addFiles)
                        files.Push(f);
               
                
                
            }       
        }
        private void threadWorkWithFile()
        {
            try
            {
                string nameDir = txtBoxNameDir.Text;
                Stack<string> dirs = new Stack<string>();

                DirectoryInfo dI = new DirectoryInfo(nameDir);
                dirs.Push(dI.FullName);
                Stack<string> files = new Stack<string>();

                while (((dirs.Count != 0) || (files.Count != 0)))
                {

                    DoOp(ref dirs, ref files);
                    
                    _pause.WaitOne();
                    if (_stop.WaitOne(0))
                    {
                        IsRun = false;
                        return;
                    }
                        
                }

                btnStart.Enabled = true;
                btnStop.Enabled = false;

                _stop.Set();
                ThreadTime.Join();
                _stop.Reset();
            }
            catch
            {
                IsRun = false;
                return;
                
            }
            IsRun = false;
        }
             
        private void AddNode(string node)
        {
          
            TreeNodeCollection tree = treeFiles.Nodes;

            TreeNode n = null;
            foreach (string str in node.Split('\\'))
            {
                bool findNode = false;
                foreach (TreeNode treeNode in tree)
                {
                    if (treeNode.Text == str)
                    {
                        tree = treeNode.Nodes;
                        findNode = true;
                    }
                }
              
                if (!findNode)
                {
                    n = new TreeNode(str);
                    tree.Add(n);
                    tree = n.Nodes;
                }
                    
            }
        }  
    }
    
}
